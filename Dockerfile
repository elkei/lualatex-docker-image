FROM ubuntu:rolling
MAINTAINER Elias Keis <2318447-elkei@users.noreply.gitlab.com>

# Install the packages
RUN apt-get update && \
	apt-get upgrade -y && \
	DEBIAN_FRONTEND=noninteractive apt-get install -y \
		texlive-luatex texlive-latex-base texlive-latex-extra texlive-fonts-recommended texlive-fonts-extra texlive-bibtex-extra texlive-extra-utils texlive-lang-german texlive-lang-english texlive-science \
		graphviz python3 python3-pip build-essential git make diffutils latexdiff tzdata wget unzip ca-certificates make latexmk biber ghostscript \
		fonts-dejavu fonts-roboto-slab fonts-roboto fonts-firacode \
		&& \
	apt-get clean

# Make the user lualatex
RUN groupadd -g 999 lualatex && \
	useradd -r -u 999 -m -s /bin/bash -g lualatex lualatex && \
	mkdir /data && \
	chown -R lualatex /data && \
	chgrp -R lualatex /data

USER lualatex
WORKDIR /data
CMD ["/bin/bash"]
